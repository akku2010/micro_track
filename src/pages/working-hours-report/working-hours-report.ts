import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
import { GeocoderProvider } from '../../providers/geocoder/geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';

@IonicPage()
@Component({
  selector: 'page-working-hours-report',
  templateUrl: 'working-hours-report.html',
})
export class WorkingHoursReportPage {

  islogin: any;
  Ignitiondevice_id: any = [];
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  igiReportData: any[] = [];
  daywiseReport: any = [];
  a: number;
  b: number;
  c: number;
  value: number;
  TotalWorkingHours: number = 0;
  device_id: any = []
  locationEndAddress: any;
  selectedVehicle: any;
  twoMonthsLater: any = moment().subtract(2, 'month').format("YYYY-MM-DD");
  today: any = moment().format("YYYY-MM-DD");

  constructor(
    public file: File,
    public socialSharing: SocialSharing,
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalldaywise: ApiServiceProvider,
    public toastCtrl: ToastController,
    private geocoderApi: GeocoderProvider) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = this.apicalldaywise.mainUrl + 'devices/getDeviceByUserDropdown?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalldaywise.startLoading().present();
    this.apicalldaywise.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalldaywise.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalldaywise.stopLoading();
          console.log(err);
        });
  }
  OnExport = function () {
    let sheet = XLSX.utils.json_to_sheet(this.igiReportData);
    let wb = {
      SheetNames: ["export"],
      Sheets: {
        "export": sheet
      }
    };

    let wbout = XLSX.write(wb, {
      bookType: 'xlsx',
      bookSST: false,
      type: 'binary'
    });

    function s2ab(s) {
      let buf = new ArrayBuffer(s.length);
      let view = new Uint8Array(buf);
      for (let i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    }

    let blob = new Blob([s2ab(wbout)], { type: 'application/octet-stream' });
    let self = this;

    // this.getStoragePath().then(function (url) {
    self.file.writeFile(self.file.dataDirectory, "ignition_report_download.xlsx", blob, { replace: true })
      .then((stuff) => {
        // alert("file downloaded at: " + self.file.dataDirectory);
        if (stuff != null) {
          // self.socialSharing.share('CSV file export', 'CSV export', url, '')
          self.socialSharing.share('CSV file export', 'CSV export', stuff['nativeURL'], '')
        } else return Promise.reject('write file')
      }).catch(() => {
        alert("error creating file at :" + self.file.dataDirectory);
      });
    // });
  }

  changeDate(key) {
    this.datetimeStart = undefined;
    if (key === 'today') {
      this.datetimeStart = moment({ hours: 0 }).format();
    } else if (key === 'yest') {
      this.datetimeStart = moment().subtract(1, 'days').format();
    } else if (key === 'week') {
      this.datetimeStart = moment().subtract(1, 'weeks').endOf('isoWeek').format();
    } else if (key === 'month') {
      this.datetimeStart = moment().startOf('month').format();
    }
  }

  getDaywisedevice(selectedVehicle) {
    console.log("inside")
    this.device_id = [];
    if (selectedVehicle.length > 0) {
      if (selectedVehicle.length > 1) {
        for (var t = 0; t < selectedVehicle.length; t++) {
          this.device_id.push(selectedVehicle[t].Device_ID)
        }
      } else {
        this.device_id.push(selectedVehicle[0].Device_ID)
      }
    } else return;
    console.log("selectedVehicle=> ", this.device_id)
  }

  daywiseReportData = [];
  getDaywiseReport() {
    let that = this;
    this.daywiseReport = []
    this.daywiseReportData = [];

    if (this.device_id == undefined) {
      this.device_id = "";
    }
    this.apicalldaywise.startLoading().present();
    this.apicalldaywise.getDaywiseReportApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.islogin._id, this.device_id)
      .subscribe(data => {
        this.apicalldaywise.stopLoading();
        // this.summaryReport = data;
        console.log("cheack daywise data", data);
        //console.log("cheack daywise data", data[i]['Moving Time']);
        this.daywiseReport = data
        console.log("recheack", this.daywiseReport);
        if (this.daywiseReport.length > 0) {
          this.innerFunc(this.daywiseReport);
        } else {
          let toast = this.toastCtrl.create({
            message: 'Report(s) not found for selected dates/vehicle.',
            duration: 1500,
            position: 'bottom'
          })
          toast.present();
        }
        // console.log("again cheack", this.innerFunc);
      }, error => {
        this.apicalldaywise.stopLoading();
        console.log(error);
      })
  }

  innerFunc(daywiseReport) {
    debugger
    let outerthis = this;
    var i = 0, howManyTimes = daywiseReport.length;
    function f() {
      outerthis.daywiseReportData.push(
        {
          'Date': moment(daywiseReport[i].Date).format('ddd MMM DD YYYY'),
          'VRN': daywiseReport[i].VRN,
          'Distance(Kms)': daywiseReport[i]["Distance(Kms)"],
          'Moving Time': daywiseReport[i]['Moving Time'],
          'Stoppage Time': daywiseReport[i]['Stoppage Time'],
          'Idle Time': daywiseReport[i]['Idle Time'],
        });
      console.log("moving time", outerthis.daywiseReport[i]['Moving Time']);
      var a = outerthis.daywiseReport[i]['Moving Time'];
      var b = outerthis.daywiseReport[i]['Idle Time']
      var c = a + b;
      console.log("added time", c);
      outerthis.daywiseReportData[i].AddedValue = c;
      var d = outerthis.daywiseReport[i]['Stoppage Time'];
      console.log("stoppage time", d);
      let e;
      if (c > d) {
        e = c - d;
      } else if (d > c) {
        e = d - c;
      }

      console.log("duration", e);
      outerthis.daywiseReportData[i].WorkingDuration = e;
      
      if (!isNaN(outerthis.daywiseReportData[i].WorkingDuration))
        outerthis.TotalWorkingHours += outerthis.daywiseReportData[i].WorkingDuration;

      console.log("total hours", outerthis.TotalWorkingHours);
      console.log("see data here: ", outerthis.daywiseReportData)
      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }
    }
    f();
  }

  millisecondConversion(duration) {
    var minutes = Math.floor((duration / (1000 * 60)) % 60);
    var hours = Math.floor((duration / (1000 * 60 * 60)) % 24);
    hours = (hours < 10) ? 0 + hours : hours;
    minutes = (minutes < 10) ? 0 + minutes : minutes;
    return hours + ":" + minutes;
  }




  timeToMins(time) {
    var b = time.split(':');
    return b[0] * 60 + +b[1];
  }

  // Convert minutes to a time in format hh:mm
  // Returned value is in range 00  to 24 hrs
  timeFromMins(mins) {
    function z(n) { return (n < 10 ? '0' : '') + n; }
    var h = (mins / 60 | 0) % 24;
    var m = mins % 60;
    return z(h) + ':' + z(m);
  }

  // Add two times in hh:mm format
  addTimes(a, b) {
    return this.timeFromMins(this.timeToMins(this.a) + this.timeToMins(this.b));
  }

}
